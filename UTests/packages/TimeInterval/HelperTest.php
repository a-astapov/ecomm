<?php
declare(strict_types = 1);
require __DIR__ . '/../../../vendor/autoload.php';
use packages\TimeInterval\Helper;

/**
 * Created by PhpStorm.
 * User: Artem
 */
class HelperTest extends \PHPUnit\Framework\TestCase
{
    public function dataProvider_testIntersect()
    {
        return [
            [20, 50, 60, 80, null], // no intersect
            [60, 80, 20, 50, null], // no intersect
            [20, 60, 50, 80, [50, 60]], // partial intersect
            [50, 80, 20, 60, [50, 60]], // partial intersect
            [20, 80, 50, 60, [50, 60]], // one in other
            [50, 60, 20, 80, [50, 60]], // one in other
            [20, 30, 25, 80, [25, 30]], // partial intersect
            [25, 80, 20, 30, [25, 30]], // partial intersect
            [80, 25, 30, 20, null], // invalid interval
            [10, 20, 30, 20, null], // invalid interval
            [80, 25, 10, 20, null], // invalid interval

            [10,10,10,10, [10,10]], //test point
            [10,10,10,20, [10,10]],
            [10,14,14,20, [14,14]],
        ];
    }

    public function dataProviderInvalidInterval()
    {
        return [
            [10, 0, \Exception::class],
            [10, -200, \Exception::class],
            ['1112', 0, \TypeError::class],
            [222, '1112', \TypeError::class],
        ];
    }

    /**
     * @param $x0
     * @param $x1
     * @param $y0
     * @param $y2
     * @param $expected_result
     * @dataProvider dataProvider_testIntersect
     */
    public function testIntersect($x0, $x1, $y0, $y2, $expected_result)
    {
        $result = Helper::getIntersect($x0, $x1, $y0, $y2);
        $this->assertEquals($expected_result, $result,
            sprintf('wrong interval on [%s, %s], [%s, %s], get %s, but expected: %s',
                $x0, $x1, $y0, $y2, is_null($result) ? 'NULL' : json_encode($result), is_null($expected_result) ? 'NULL' : json_encode($expected_result)));
    }


    /**
     * @param $x0
     * @param $x1
     * @param $exception
     * @dataProvider dataProviderInvalidInterval
     */
    public function testExceptionInvalidInterval($x0, $x1, $exception)
    {
        $this->expectException($exception);
        Helper::checkInterval($x0, $x1);
    }
}