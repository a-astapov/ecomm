<?php
/**
 * Created by PhpStorm.
 * User: Artem
 */
namespace packages\TimeInterval;

class Helper
{
    /**
     * @param int $x0
     * @param int $x1
     * @param int $y0
     * @param int $y1
     * @return array|null
     */
    public static function getIntersect(int $x0,int $x1,int $y0,int $y1)
    {
        if ($x0 <= $y1 && $y0 <= $x1) {
            return [max($x0, $y0), min($x1, $y1)];
        }
        return null;
    }


    /**
     * @param int $x0
     * @param int $x1
     * @throws \Exception
     */
    public static function checkInterval(int $x0,int $x1)
    {
        if ($x0 >= $x1) throw new \Exception(sprintf('Wrong interval [%d, %d]', $x0, $x1));
    }
}